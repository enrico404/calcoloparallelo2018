
#include <stdio.h>
#include "mpi.h"

int main(int argc, char const *argv[])
{
    int rank, world_size;
    int token;
    int ierr;
    int status;
    
    MPI_Init( &argc, &argv );
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank == 0){
        token = 10;
        MPI_Send(&token, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD);
        printf("process: %d, sent the token: %d\n",rank, token);
        
        

    }
    
    if(rank != 0){
        if(rank == world_size-1){
            int next =  rank - (world_size-1);
            int ierr = MPI_Recv(&token, 1, MPI_INT, rank-1 , 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("Process %d received token %d from process %d\n", rank, token, rank-1);
            if(ierr == MPI_SUCCESS)
            MPI_Send(&token, 1, MPI_INT, next, 0, MPI_COMM_WORLD);
              

            
        }else{
            MPI_Recv(&token, 1, MPI_INT, rank-1 , 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("Process %d received token %d from process %d\n", rank, token, rank-1);
            MPI_Send(&token, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD);

        }
      
        
       
        
    }
    if(rank== 0){
        MPI_Recv(&token, 1, MPI_INT, world_size-1 , 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d received token %d from process %d\n", rank, token, world_size-1);
    }

    MPI_Finalize();
    return 0;
}
