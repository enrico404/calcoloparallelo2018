#include <stdio.h>
#include <omp.h>

int main(int argc, char const *argv[])
{
    int c = 0;
    //data race
    omp_lock_t lock;
    omp_init_lock(&lock);
    #pragma omp parallel num_threads(8) shared(c)
        {
            //la set è una chiamata bloccante, ovvero non garantisce di uscirne mai
            omp_set_lock(&lock);
            //critical section
            printf("before modify c thread: %d \n", omp_get_thread_num());
            c++;
            printf("after modify c thread: %d \n", omp_get_thread_num());
            // unset contiene anche una chiamata a flush, per scaricare la cache sulla memria principale e rendere consistenti i dati 
            omp_unset_lock(&lock);
            
        }
    omp_destroy_lock(&lock);
    printf("valore di c after pararreg : %d \n", c);
    return 0;
}
