#include<stdio.h>
#include<cuda.h>
#include <time.h>

#define N 1000000000


void add(int *a, int *b, int *c){
	for(int i=0; i<N; i++){
        c[i] = a[i]+b[i];
    }
}

void stampaVec(int *c){
	for(int i=0; i<N; i++){
	printf("%d ", c[i]);
	}
	printf("\n");


}
int main(){
	int *a,*b,*c;
	int *d_a, *d_b, *d_c;
    clock_t start, diff;
	long long int size = N*sizeof(int);

	a = (int *) malloc(size);
	b = (int *) malloc(size);
	c = (int *) malloc(size);
	
	
	//alloco la memoria sulla gpu

    for(int i=0; i<N; i++){
		a[i] = i;
		b[i] = i;
	}
	start = clock();
    add(a,b,c);
    diff = clock() - start;
  
     int msec = diff * 1000 / CLOCKS_PER_SEC;
   printf("Time taken %d ms \n", msec);
/* 	printf("vettore c: \n");
	stampaVec(c);	
	printf("vettore a: \n");
	stampaVec(a);	
	printf("vettore b: \n");
	stampaVec(b);	 */

	free(a);
	free(b);
	free(c);

	return 0;

}
