#include <stdio.h>
#include <omp.h>

void t0(){
    printf(" 0 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}

void t1(){
    printf(" 1 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}
void t2(){
    printf(" 2 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}
int main(){
#pragma omp parallel num_threads(2)
{
    
    //va bene anche usare la sections
    #pragma omp single
    {   
        //il thread principale aspetta fino a quando tutti i task non sono stati completati
       
    
            int x = 0;
            //i lock sono gestito in automatico, in:x aspetta fino a quando out:x non finisce
            //l'out deve essere sempre prima dell'in, altrimenti in aspetta nessuno
           
             #pragma omp task depend(out:x)
            {
                t1();
                x = 10;
                
                
            }
             #pragma omp task depend(in:x)
            {
                t0();
                  
                
            }
           
            
        

        
        
        //task yield: punto in cui consumo un task dalla coda
        //task wait: aspetto che un task finisca

      
    }//implicit barrier
    
}//implicit barrier



return 0;
}