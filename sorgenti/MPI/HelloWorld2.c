#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[]){
    int rank;
    int size, num;
    MPI_Status stat;
    MPI_Init(&argc, &argv);
    //ottengo la dimensione del mio mondo
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    //in ogni processo vado a salvarmi il suo rank nella var rank
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank==0){
        printf("Hello world from process: %d, inserisci il valore che vuoi mandare al processo numero 1: \n", rank);
        scanf("%d", &num);
        MPI_Send(&num, 1, MPI_INT, 1, 15, MPI_COMM_WORLD );
    }
    if(rank==1 ){
        MPI_Recv(&num, 1, MPI_INT, 0, 15, MPI_COMM_WORLD, &stat);
        printf("Hello world from process: %d,ho ricevuto il valore : %d\n", rank, num);
    }

    MPI_Finalize();
    return 0;
}

