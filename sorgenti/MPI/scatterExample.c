#include <stdio.h>
#include "mpi.h"
int main(int argc, char const *argv[])
{   
    int rank, world_size;
    MPI_Init(&argc ,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);


    int BeforeScatter[5];
    int AfterScatter;
    if(rank ==0){
        BeforeScatter[0]=10;
        BeforeScatter[1]=10;
        BeforeScatter[2]=140;
        BeforeScatter[3]=20;
        BeforeScatter[4]=30;
        BeforeScatter[5]=40;


    }

    MPI_Scatter(&BeforeScatter, 1, MPI_INT, &AfterScatter, 1, MPI_INT, 0, MPI_COMM_WORLD );
    MPI_Barrier(MPI_COMM_WORLD);
    int gather[10];
    MPI_Gather(&AfterScatter, 1, MPI_INT, &gather, 1, MPI_INT, 0 , MPI_COMM_WORLD);


    printf("proc: %d, ha ricevuto: %d\n", rank, AfterScatter);
    if(rank==0){
        for(int i=0; i< 6; i++){
            printf("%d ", gather[i]);
        }


    }
   
    MPI_Finalize();
    return 0;
}
