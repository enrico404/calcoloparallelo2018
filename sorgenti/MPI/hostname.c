#include <stdio.h>
#include "mpi.h"

int main(int argc, char const *argv[])
{
    MPI_Init(&argc, &argv);
    char name[50];
    int length;
    MPI_Get_processor_name(&name, &length);
    printf("nome proc: %s, len: %d\n", name, length);


    MPI_Finalize();
    return 0;
}

