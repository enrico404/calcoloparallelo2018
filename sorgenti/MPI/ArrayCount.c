#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>
#include "time.h"

#define MAX_LENGTH 1000
int main( int argc, char *argv[] )
{
    int rank, size;
    MPI_Status stat;
    MPI_Init( &argc, &argv );
    MPI_Comm_size( MPI_COMM_WORLD, &size );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    srand(time(0));
   
    int arr[MAX_LENGTH];
    if(rank==0){
        int N = rand()%50;
        for(int i=0; i<N; i++){
            arr[i] = rand()%100;
        }
        MPI_Send(&arr, N, MPI_INT, 1, 15, MPI_COMM_WORLD);
    }
    if(rank==1){
        //il secondo valore indica solo il massimo della lunghezza ricevibile
        MPI_Recv(&arr, MAX_LENGTH, MPI_INT, 0, 15, MPI_COMM_WORLD, &stat);
        int dim;
        //conta il numero di entry ricevute e le mette in dim 
        MPI_Get_count(&stat, MPI_INT, &dim);
        printf("processo %d, array ricevuto: \n",rank);
        for(int i=0; i<dim; i++){
            printf("%d ", arr[i]);
        }
        printf("\n");

        printf("dimensione array: %d \n", dim);
     
    }


    MPI_Finalize();
    return 0;
}