#include <stdio.h>
#include <omp.h>

int main(int argc, char const *argv[])
{

    int counter = 1;
    //esegue il codice poi moltiplica tutti i counter dei vari thread, deve essere shared per forza 
    //perchè altrimenti ognuno lavora sulla propria variabile privata e non si vede all'esterno
    #pragma omp parallel num_threads(4) reduction(+:counter)
        { //fork
        //implicitamente fa: int counter=1 qui dentro
         counter = 2;
     
         printf("valore di counter: %d \n", counter);
         
        } // implicit barrier and join
        // 1*2*2*2*2
        printf("valore di counter: %d \n", counter);
    return 0;
}
