#include <stdio.h>
#include <omp.h>

int main(int argc, char const *argv[])
{

    int a=10;
    #pragma omp parallel 
        { //fork

        //a lo setta a 0 inizialmente e poi lo riporta fuori
        #pragma omp for lastprivate(a)
            for(int i=0; i<5; i++){
                a += i;
            }
        #pragma omp single
        printf("valore di a: %d\n", a);

  
        } // implicit barrier and join
    printf("valore esterno di a: %d\n", a);
    return 0;
}
