#include <stdio.h>
#include <omp.h>

int main(int argc, char const *argv[])
{
    omp_lock_t lock;
    omp_init_lock(&lock);
    int a = 0;
    #pragma omp parallel shared(a) num_threads(4)
    {
        omp_set_lock(&lock);
        
        a++;
        printf("[%d] incremento a... : %d\n", omp_get_thread_num(), a);
        omp_unset_lock(&lock);
    }
    
    return 0;
}
