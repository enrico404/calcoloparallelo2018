#include <stdio.h>
#include <omp.h>

int main(int argc, char const *argv[])
{
    #pragma omp target 
        printf("Hello world from device thread: %d\n", omp_get_num_threads());
    
    return 0;
}
