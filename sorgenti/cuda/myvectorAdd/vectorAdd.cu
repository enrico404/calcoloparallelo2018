#include<stdio.h>
#include<cuda.h>
#include <time.h>

#define N 1000000000
#define THREAD_PER_BLOCK 512

__global__ void add(int *a, int *b, int *c){
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	c[index] = a[index]+b[index];

}

void stampaVec(int *c){
	for(int i=0; i<N; i++){
	printf("%d ", c[i]);
	}
	printf("\n");


}
int main(){
	int *a,*b,*c;
	int *d_a, *d_b, *d_c;
	long int size = N*sizeof(int);
	clock_t start, diff;

	a = (int *) malloc(size);
	b = (int *) malloc(size);
	c = (int *) malloc(size);
	
	
	//alloco la memoria sulla gpu
	cudaMalloc((void **)&d_a, size);
	cudaMalloc((void **)&d_b, size);
	cudaMalloc((void **)&d_c, size);
    for(int i=0; i<N; i++){
		a[i] = i;
		b[i] = i;
	}
	start = clock();
	cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);
	//lancio l'addizione sulla gpu
	
	add<<<N/THREAD_PER_BLOCK, THREAD_PER_BLOCK>>>(d_a, d_b, d_c);
	//copy the result back to host
	cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);
	diff = clock() - start;
  
     int msec = diff * 1000 / CLOCKS_PER_SEC;
   printf("Time taken %d ms \n", msec);
/* 	printf("vettore c: \n");
	stampaVec(c);	
	printf("vettore a: \n");
	stampaVec(a);	
	printf("vettore b: \n");
	stampaVec(b);	 */

	//pulisco la memoria
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);
	free(a);
	free(b);
	free(c);
	return 0;

}
