#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>


//3k*3k -> 110ms su gtx 1060
#define ROWS 2
#define COLS 2
//10k*10k = 4sec

#define TILE_WIDTH 16
__global__ void molt(int *A, int *B, int *C){

    int cvalue = 0;
   // non si può allocare con il malloc
/*     __shared__ int *tA = (int*) malloc(TILE_WIDTH*TILE_WIDTH*sizeof(int)) ;
    __shared__ int *tB = (int*) malloc(TILE_WIDTH*TILE_WIDTH*sizeof(int)) ; */
    __shared__ int tA[TILE_WIDTH][TILE_WIDTH];
    __shared__ int tB[TILE_WIDTH][TILE_WIDTH];

    
    int tx = threadIdx.x, ty = threadIdx.y;

    //calcolo le coordinate della tile
    int row = blockIdx.y*TILE_WIDTH+ty;
    int col = blockIdx.x*TILE_WIDTH+tx;


    //per ogni tile.. mi serve l'indice i per beccare la tile giusta all'interno
    //della matrice A e B, inoltre in cvalue è accumulato il prodotto dei valori delle tiles
   //in questo modo terminate tutte le tiles disponibili ho il valore corretto in cvalue trovato come al solito
	// riga per colonna
    for(int i=0; i<(COLS-1)/TILE_WIDTH+1; i++){
        //se la tiles è al'interno della matrice -> salvo il valore nella shared memory
        //per la tiles a uso la matrice a
        if(row < ROWS && i*TILE_WIDTH+tx < COLS ){
            tA[ty][tx] = A[row*COLS+i*TILE_WIDTH+tx];
        }else tA[ty][tx] = 0;

        //per la tiles b uso B
        if(col < COLS && i*TILE_WIDTH+ty < ROWS){
            tB[ty][tx]= B[(i*TILE_WIDTH+ty)*COLS+col];
        }else tB[ty][tx]=0;
        //barrier per i thread nei blocchi
        __syncthreads();

        for(int i=0; i<TILE_WIDTH; i++){
            cvalue += tA[ty][i]*tB[i][tx];
        }
        //aspetto che tutti i blocchi fanno il conto
        __syncthreads();
    }
    //se la tile è all'interno della matrice
    if(row < ROWS && col < COLS){
        C[row*COLS+col] = cvalue;
    }


  
}

void stampa_matrice(int *m){

    for(int i=0;i<ROWS; i++){
        for(int j=0;j<COLS; j++){
            printf("%d ", m[i*ROWS+j]);
        }
        printf("\n");
    }


}

int main(int argc, char const *argv[])
{

        
    int *m1 = (int*)malloc(sizeof(int)*ROWS*COLS);
    int *m2 = (int*)malloc(sizeof(int)*ROWS*COLS);
    int *c  = (int*)malloc(sizeof(int)*ROWS*COLS);

    int *d_m1;
    int *d_m2;
    int *d_c;

    if(ROWS == COLS){


            for(int i=0; i<ROWS; i++){
                for(int j=0; j<COLS; j++){

                    m1[i*ROWS+j] = i+j;
                    m2[i*ROWS+j] = 2*(i+j);
                }
            }
        
        


        cudaMalloc((void**) &d_m1, ROWS*COLS*sizeof(int));
        cudaMalloc((void**) &d_m2, ROWS*COLS*sizeof(int));
        cudaMalloc((void**) &d_c, ROWS*COLS*sizeof(int));

        cudaMemcpy(d_m1, m1, ROWS*COLS*sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_m2, m2, ROWS*COLS*sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_c, c, ROWS*COLS*sizeof(int), cudaMemcpyHostToDevice);

        float threadPerBlock = 16;
        dim3 dimGrid(ceil(ROWS/threadPerBlock), ceil(COLS/threadPerBlock));
        dim3 dimBlock(threadPerBlock, threadPerBlock, 1);

        molt<<<dimGrid, dimBlock>>>(d_m1,d_m2,d_c);

        cudaDeviceSynchronize();
        cudaMemcpy(c, d_c, ROWS*COLS*sizeof(int), cudaMemcpyDeviceToHost);

         printf("matrice 1: \n");
        stampa_matrice(m1);
        printf("matrice 2: \n");
        stampa_matrice(m2);
        printf("matrice risultante: \n");
        stampa_matrice(c); 





        cudaFree(d_m1);
        cudaFree(d_m2);
        cudaFree(d_c);
        free(m1);
        free(m2);
        free(c);
    }
    return 0;
}
