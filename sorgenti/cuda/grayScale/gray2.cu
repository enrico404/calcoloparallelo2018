#include<stdio.h>
#include<cuda.h>
#include <time.h>
#include <stdlib.h>

#define CHANNELS 3






__global__ void colorConvert(unsigned char *d_grayImage, unsigned char *d_rgbImage, int width, int height){

    int x = threadIdx.x+blockIdx.x*blockDim.x;
    int y = threadIdx.y+blockIdx.y*blockDim.y;

    if(x<width && y<height){
     
        int grayOffset = y*width+x;

        int rgbOffset = grayOffset*CHANNELS;
        unsigned char r = d_rgbImage[rgbOffset];
        unsigned char b = d_rgbImage[rgbOffset+1];
        unsigned char g = d_rgbImage[rgbOffset+2];

        d_grayImage[grayOffset] = 0.21f*r + 0.71f*g + 0.07f*b;
        
    
  

    }


}
int main(int argc, char *argv[]){


    FILE *f;
    FILE *f2;
    char *s = argv[1];
    if(argv[1]== NULL){
        printf("please insert the name of the file you want to convert... \n");
        return 1;
    }
    s = strcat(s, ".ppm");
    f = fopen(s, "rb");
    f2 = fopen("res.ppm", "wb");
    if(f ==NULL){
        printf("Error on opening ppm image");
        return 1;
    }


    int width, height;
    unsigned char *grayImage;
    unsigned char *rgbImage;
    unsigned char *d_rgbImage, *d_grayImage;
    int size;
    
    int read = fscanf(f, "P6 %d %d 255\n", &width, &height);
    
    size = width*height*CHANNELS;
          
    rgbImage = new unsigned char[size];
    grayImage  = new unsigned char[size];
    d_rgbImage = new unsigned char[size];
    d_grayImage  = new unsigned char[size];

    fread(rgbImage, sizeof(unsigned char), size, f);

    
    cudaMalloc((void**) &d_grayImage, size);
    cudaMalloc((void**) &d_rgbImage, size);

    cudaMemcpy(d_grayImage, grayImage, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_rgbImage, rgbImage, size, cudaMemcpyHostToDevice);
    
    
    

    dim3 dimGrid(ceil((float)width / 16),
    ceil((float)height / 16));
    dim3 dimBlock(16, 16, 1);

    colorConvert<<<dimGrid,dimBlock>>>(d_grayImage, d_rgbImage, width, height); 


    cudaMemcpy(grayImage, d_grayImage, size, cudaMemcpyDeviceToHost);

    fprintf(f2, "P6\n%d %d\n255\n", width, height);
    for(int i=0; i< size; i++){
        /* printf("%u ", grayImage[i]); */
        //va scritto in char
        fprintf(f2, "%c", grayImage[i]); 
      
    }
       

    cudaFree(d_grayImage);
    cudaFree(d_rgbImage);

    fclose(f2);
    fclose(f);

return 0;
}