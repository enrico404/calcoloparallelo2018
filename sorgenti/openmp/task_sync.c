#include <stdio.h>
#include <omp.h>

void t0(){
    printf(" 0 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}

void t1(){
    printf(" 1 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}
void t2(){
    printf(" 2 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}
int main(){
#pragma omp parallel num_threads(2)
{
    omp_lock_t lock;
    //va bene anche usare la sections
    #pragma omp single
    {   omp_init_lock(&lock);
        omp_set_lock(&lock);
        //il thread principale aspetta fino a quando tutti i task non sono stati completati
        #pragma omp taskgroup
        {
            #pragma omp task
            {
                //rimango bloccato fino a quando non lo sblocco
                //il prblema è quando ho un solo thread ce viene continuamente rischedulato e aspetta
                //che qualcun altro lo sblocchi ma non succederà
                //sol fai un ciclo while con dentro una task yield
                omp_set_lock(&lock);
                t0();


            }
            #pragma omp task
            {
                t1();
                //release
                omp_unset_lock(&lock);
            }
            
            #pragma omp task
            t2();

        }
        
        //task yield: punto in cui consumo un task dalla coda
        //task wait: aspetto che un task finisca

      
    }//implicit barrier
    
}//implicit barrier



return 0;
}