#include <stdio.h>
#include <omp.h>

void t0(){
    printf("*************** I am thread %d and I am executing t0\n", omp_get_thread_num());
}
void t1(){
    printf("*************** I am thread %d and I am executing t1\n", omp_get_thread_num());
}
void t2(){
    printf("*************** I am thread %d and I am executing t2\n", omp_get_thread_num());
}
void t3(){
    printf("*************** I am thread %d and I am executing t3\n", omp_get_thread_num());
}
void t4(){
    printf("*************** I am thread %d and I am executing t4\n", omp_get_thread_num());
}
int main()
{
    omp_lock_t lock;

	#pragma omp parallel 
	{
		#pragma omp single
		{
            omp_init_lock(&lock);
            omp_set_lock(&lock);
            int x=0;
            int y=0;
            //i task nella taskgroup sono eseguiti in blocco, siccome viene prima la taskgroup
            //verrà eseguita per prima, siccome alla fine della taskgroup questi devono
            //venire consumati per uscire
            #pragma omp taskgroup
            {
                #pragma omp task depend(out:x)
                    t1();
            

                #pragma omp task depend(in:x) depend(out:y)
                    t0();

                #pragma omp task depend(in:y)
                    t4();


            }
           
                
                
            #pragma omp task
            t2();

            #pragma omp task
            t3();
			
		}
	} // barrier
	
	return 0;
}