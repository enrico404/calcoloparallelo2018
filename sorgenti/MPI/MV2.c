#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

#define master 0
#define tag 15


//matrice * matrice funzionante fatto da me MPI, più veloce openmp -> mat 3k*3k omp 17sec (su fisso), questo 48sec

void stampaM(int *m, int rows, int cols){


    for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){

            printf("%d ", m[i*rows+j]);

        }
        printf("\n");
    }

}


void init_mat(int *m, int rows, int cols){
     
       for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){

            m[i*rows+j] = i+j+ rand()%10;

        }
    }

}



int main(int argc, char *argv[])
{
    int dim = 3000;
    int rows_a = dim;
    int cols_a = dim;
    int rows_b = dim;
    int cols_b = dim;

    int rows_c = rows_a;
    int cols_c = cols_b;
    int rank, size;
    MPI_Status status; 
    int *a, *b, *c;
    int *proc_rows = malloc(sizeof(int)*cols_a);
    int *c_rows = malloc(sizeof(int)*cols_c);
    srand(time(0));
   
    int work = 1;
    if(cols_a == rows_b){
        
        a = (int *)malloc(sizeof(int)*rows_a*cols_a);
        b = (int *)malloc(sizeof(int)*rows_b*cols_b);
        c =(int *) malloc(sizeof(int)*rows_c*cols_c);

        MPI_Init(&argc, &argv);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Comm_size(MPI_COMM_WORLD, &size);

        if(rank == master){
           

            init_mat(a, rows_a, cols_a);
            init_mat(b, rows_b, cols_b);

/* 
            printf("A: \n");
            stampaM(a, rows_a, cols_a);
            printf("b: \n");
            stampaM(b, rows_b, cols_b); */
            int k=1;
            for(int i=0; i<rows_a; i++){
                    int n = (k++)%size;
                    if(n==0) n = (k++)%size; 
                    MPI_Send(&a[i*rows_a], cols_a, MPI_INT, n, tag, MPI_COMM_WORLD);
                    
                   
            } 
            
  
           
        }

       // MPI_Scatter(&a[0], cols_a, MPI_INT,  &proc_rows[0], cols_a, MPI_INT, master, MPI_COMM_WORLD );
        MPI_Bcast(&b[0], rows_b*cols_b, MPI_INT, master, MPI_COMM_WORLD);
        if(rank != master && rank <= rows_a){
            //receive in broadcast mode the second matrix, beacause all proc need it to calculate the product
          //  MPI_Bcast(&b[0], rows_b*cols_b, MPI_INT, master, MPI_COMM_WORLD);
            while(1){
              
               MPI_Recv(&proc_rows[0], cols_a, MPI_INT, master, tag, MPI_COMM_WORLD, &status);
               

                    for(int i=0; i<cols_c; i++) c_rows[i] =0;
                   
                   int tmp; 
                    for(int j=0; j<cols_b;j++){

                        for(int i=0, k=0; i< cols_a; i++, k++){
                              
                             c_rows[j] += proc_rows[i]*b[k*rows_b+j];
                            //printf("%d * %d + ",proc_rows[i],b[k*rows_b+j]);
                          
                        }
                      //  printf("\n ");
                        
                        
                           
                        

                    
                    //printf("%d ", c_rows[i]);

                }
          
                
                MPI_Send(&c_rows[0], cols_c, MPI_INT, master, tag, MPI_COMM_WORLD);
                 //printf("\n ");
            }
              //   MPI_Gather(&c_rows[0], cols_c, MPI_INT, &c[0], cols_c, MPI_INT, master, MPI_COMM_WORLD);
           

        }

  
        
    if(rank == 0){
        int k= 1;
          for(int i=0; i< rows_c; i++){
                int n = (k++)%size;
                if(n==0) n = (k++)%size; 
                MPI_Recv(&c_rows[0], cols_a, MPI_INT, n , tag, MPI_COMM_WORLD, &status);
                for(int j=0; j<cols_c; j++){
                     c[i*rows_c+j] = c_rows[j]; 
                }
               

            } 
           // MPI_Gather(&c_rows[0], cols_c, MPI_INT, &c[0], cols_c, MPI_INT, master, MPI_COMM_WORLD);
          /*   printf("stampo c: \n");
            stampaM(c, rows_c, cols_c); */
            MPI_Abort(MPI_COMM_WORLD, 1);
    }
        MPI_Finalize();

    }


    return 0;
}
