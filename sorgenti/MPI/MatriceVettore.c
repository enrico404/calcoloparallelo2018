#include <stdio.h>
#include "mpi.h"

#define ROWS 5
#define COLS 5

void stampa(int M[][COLS]){
    printf("stampo la matrice... \n");
    for(int i=0; i<ROWS; i++){
        for(int j=0; j<COLS; j++){
            printf("%d ", M[i][j]);
        }
        printf("\n");
    }


}


void stampaV(int V[]){
    printf("stampo il vettore... \n");
    for(int i=0; i<ROWS; i++)
        printf("%d ", V[i]);
    printf("\n");

}

int main(int argc, char const *argv[])
{
    int rank, world_size;
      /* inizializzo matrice e vettore */
   
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    //devo controllare se il numero dei processori è uguale alla grandeza della matrice, altimenti abort
   /*  if(world_size != (ROWS+1)){
        int tmp = ROWS+1;
        printf("Must use %d processes for this program\n", tmp);
        MPI_Abort(MPI_COMM_WORLD, 1);
        return;
    } */


  
    //master id
    int master =0;
    int b[ROWS];
    int M[ROWS][COLS];
/*     int M[ROWS][COLS] = {{1,2,3},
                        {4,5,6},
                        {7,8,9}}; */
    for(int i=0; i<ROWS; i++){
        for(int j=0; j<COLS; j++){
            M[i][j] = i+j;
        }

    }
   
     for(int i=0; i<ROWS; i++){
        b[i]= i+1;
        
    }

    int tmp[COLS];
    // if i am the master process
    if(rank  == master){

        stampa(M);
        stampaV(b);
        //send the vector to all slaves
        MPI_Bcast(&b, ROWS, MPI_INT, master, MPI_COMM_WORLD );
        //send each row to a different slave process
        int next = 0;

            for(int j=0; j<ROWS; j++){
                next = (next+1) % world_size;
                if(next == 0) next = 1;
                printf("mando a processore: %d \n", next);

                for(int k=0; k<COLS; k++){
                     tmp[k] = M[j][k];

                     
                }
               MPI_Send(&tmp, COLS, MPI_INT, next, 0, MPI_COMM_WORLD);
               
            }
    int result=0;
    int ResM[ROWS][1];
    int src = 0;
    printf("risultato dell'operazione: \n");
    for(int i =0; i<ROWS; i++){
        src = (src+1) % world_size;
        if(src ==0 ) src =1;
        printf("ricevo da processore: %d \n", src);
        MPI_Recv(&result, 1, MPI_INT, src, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE );
        ResM[i][1] = result;
        
        printf("%d \n", ResM[i][1]);
        //if i've finished the rows -> abort
        if((i+1)==ROWS){MPI_Abort(MPI_COMM_WORLD, 10);} 
        

        

    }
    
    
    
   /*  printf("stampo la matrice... \n");
    for(int i=0; i<ROWS; i++){
        for(int j=0; j<1; j++){
            printf("%d ", ResM[i][j]);
        }
        printf("\n");
    } */

    }  

    // into the slave's process  
   if(rank != 0){
       
       //copy the vector into buffer
       int buffer[ROWS];
       MPI_Bcast(&buffer, ROWS, MPI_INT, master, MPI_COMM_WORLD );
       int MatRow[COLS];
       while(1){
            MPI_Recv(&MatRow, COLS, MPI_INT, master, 0 , MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            int res = 0;
            for(int i=0; i<COLS; i++){
                res += MatRow[i]*buffer[i];
                
            }
            MPI_Send(&res, 1, MPI_INT, master, 0, MPI_COMM_WORLD);
       }

   }


    

    MPI_Finalize();

    return 0;
}
