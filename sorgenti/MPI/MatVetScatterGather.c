#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define ROWS 3
#define COLS 3



/* 
void stampaM(int *M[COLS]){
    for(int i=0; i<ROWS; i++){
            
            for(int j=0; j<COLS; j++){
                printf("%d ", M[i][j]);
            }
            printf("\n");
        }

}  */



void stampaV(int V[], int size){
    printf("vettore ricevuto: \n");
    for(int i=0; i<size; i++)
        printf("%d ", V[i]);
    printf("\n");

}


int main(int argc, char const *argv[])
{
   
    int world_size, rank;
   
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int master = 0;
  
    int test;
    int proc_row[COLS];
    int b[ROWS];
    int m[ROWS][COLS];


    if( rank == master){
        //se sono nel master inizializzo vettore e matrice
        for(int i=0; i<ROWS; i++){
            
            for(int j=0; j<COLS; j++){
                m[i][j] = i+j;
                b[i]= i*j;
            }
        }


        
  
   
    }
    //mando in broadcast il vettore b
     MPI_Bcast(&b, COLS, MPI_DOUBLE_PRECISION, master, MPI_COMM_WORLD); 
   
   //divide the matrix 
   
    MPI_Scatter(m, ROWS, MPI_INT, &proc_row,ROWS, MPI_INT, master, MPI_COMM_WORLD);

    printf("proc: %d,\n", rank);
 
         stampaV(proc_row, ROWS);

    int c=0;
    for(int i=0; i<ROWS; i++){
        
            c +=  proc_row[i]*b[i];
           
        
    }
     printf("proc: %d, valore di c: %d \n",rank, c);
    int vect[ROWS];
    MPI_Gather(&c, 1, MPI_INT, vect, 1, MPI_INT, master, MPI_COMM_WORLD);
    if(rank ==0){

        printf("vettore risultate... \n");
        stampaV(vect, ROWS);

    }

    MPI_Finalize();
    
    return 0;
}
