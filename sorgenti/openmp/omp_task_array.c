#include <stdio.h>
#include <omp.h>
#define N 10



int main(){

    int A[N];
    #pragma omp parallel
    {
       // #pragma omp for schedule(dynamic, 1)
        #pragma omp single
        {
            printf("*********** i'm the thread: %d and i'm push the tasks \n", omp_get_thread_num());
            for(int i=0; i<16; i++){
                #pragma omp task firstprivate(i)
                {
                    printf("i'm the thread %d and i'm executing task: %d \n", omp_get_thread_num(), i);
                }
            
        }
        }
      
        
    }



return 0;
}