#include <stdio.h>
#include <omp.h>

void t0(){
    printf(" 0 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}

void t1(){
    printf(" 1 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}
void t2(){
    printf(" 2 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}
int main(){

omp_lock_t lock;
omp_init_lock(&lock);

#pragma omp parallel num_threads(2)
{
 
    #pragma omp single
    {   
  
            
    omp_set_lock(&lock);   
           
             #pragma omp task 
             {
                 while(!omp_test_lock(&lock)){
                     #pragma omp taskyield
                 }
                   t0();
             }
              
              
    
             #pragma omp task 
             {
                  t1();
                  omp_unset_lock(&lock);

             }
               


      
    }//implicit barrier
    
}//implicit barrier



return 0;
}