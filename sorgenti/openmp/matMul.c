#include <stdio.h>
#include <omp.h>

#define ROWS 3000
#define COLS 3000

#include <time.h>



//matrice * matrice funzionante fatto da me OPENMP
void stampa_matrice(int *m){

    for(int i=0;i<ROWS; i++){
        for(int j=0;j<COLS; j++){
            printf("%d ", m[i*ROWS+j]);
        }
        printf("\n");
    }


}


int* molt(int *m1, int* m2){
    clock_t start, end;
    double cpu_time;
    int *c  = malloc(sizeof(int)*ROWS*COLS);

     for(int i=0;i<ROWS; i++){
        for(int j=0;j<COLS; j++){
            c[i*ROWS+j] = 0;
        }
        
    }
    start = clock();
    #pragma omp parallel for schedule(dynamic, ROWS/4)
    for(int i=0; i<ROWS; i++){
        for(int k=0; k<COLS; k++){
            for(int j=0; j<COLS; j++){
                c[i*ROWS+j] += m1[i*ROWS+k]*m2[k*ROWS+j];
            }
        }
    }
    end = clock();
    cpu_time = ((double) (end-start))/CLOCKS_PER_SEC;
    printf("time elapsed:usa il comando time\n", cpu_time);



    return c;
}
int main(int argc, char const *argv[])
{
    
    int *m1 = malloc(sizeof(int)*ROWS*COLS);
    int *m2 = malloc(sizeof(int)*ROWS*COLS);
    int *c  = malloc(sizeof(int)*ROWS*COLS);

    if(ROWS == COLS){


        for(int i=0; i<ROWS; i++){
            for(int j=0; j<COLS; j++){

                m1[i*ROWS+j] = i+j;
                m2[i*ROWS+j] = 2*(i+j);
            }
        }

        c = molt(m1,m2);
        /* printf("matrice 1: \n");
        stampa_matrice(m1);
        printf("matrice 2: \n");
        stampa_matrice(m2);
        printf("matrice risultante: \n");
        stampa_matrice(c); */

    }
    return 0;
}
