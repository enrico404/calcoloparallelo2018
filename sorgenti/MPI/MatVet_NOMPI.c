#include <stdio.h>

#define ROWS 3
#define COLS 3

void stampa(int M[][COLS]){
    printf("stampo la matrice... \n");
    for(int i=0; i<ROWS; i++){
        for(int j=0; j<COLS; j++){
            printf("%d ", M[i][j]);
        }
        printf("\n");
    }


}


void stampaV(int V[]){
    printf("stampo il vettore... \n");
    for(int i=0; i<ROWS; i++)
        printf("%d\n", V[i]);
    printf("\n");

}


int main(int argc, char const *argv[])
{
    int b[ROWS];
    int M[ROWS][COLS];
    //genero matrice e vettore
    for(int i=0; i<ROWS; i++){
        for(int j=0; j<COLS; j++){
            M[i][j] = i+j;
        }

    }
   
     for(int i=0; i<ROWS; i++){
        b[i]= i+1;
        
    }
    int res[ROWS];
    int tmp;
    stampa(M);
        stampaV(b);
    for(int i=0; i<ROWS; i++){
        tmp=0;
        for(int j=0; j<COLS; j++){
            tmp += M[i][j]*b[j];
        }
        res[i]=tmp;
    }

    stampaV(res);
    return 0;
}
