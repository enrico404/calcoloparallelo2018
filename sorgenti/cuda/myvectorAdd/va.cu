#include<stdio.h>
#include<cuda.h>
#include <time.h>


#define THREAD_PER_BLOCK 2

__global__ void add(int *a, int *b, int *c, int n){
    int index = threadIdx.x + blockDim.x * blockIdx.x;
    if(index < n)
	    c[index] = a[index]+b[index];

}

__global__ void stam(){
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    printf("hello world %d\n", i);
}

void stampaVec(int *c, int n){
	for(int i=0; i<n; i++){
	printf("%d ", c[i]);
	}
	printf("\n");


}


void veccAdd(int *a, int *b, int *c, int n){
    int size = n*sizeof(int);
    int *d_a, *d_b, *d_c;

    //alloco memoria su gpu
    cudaMalloc((void**) &d_a, size);
    cudaMalloc((void**) &d_b, size);
    cudaMalloc((void**) &d_c, size);

    //copio la memoria da host a device (gpu) prende gli indirizzi e non il contenuto con il reference
    cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);
    //numero di blocchi e threads per blocco
	add<<<ceil(n/THREAD_PER_BLOCK), THREAD_PER_BLOCK>>>(d_a, d_b, d_c, n);
	stam<<<3, 2>>>();
	
	cudaDeviceSynchronize();
    cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);


    printf("vettore c: \n");
	stampaVec(c,n);	
	printf("vettore a: \n");
	stampaVec(a,n);	
	printf("vettore b: \n");
    stampaVec(b,n);	
    
    
    //pulisco la memoria
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);


}
int main(){
	int *a,*b,*c;
    int n = 10;
	long int size = n*sizeof(int);

    //alloco memoria su host
	a = (int *) malloc(size);
	b = (int *) malloc(size);
	c = (int *) malloc(size);
	
	//inizializzo vettori
    for(int i=0; i<n; i++){
		a[i] = i;
		b[i] = i;
    }
    veccAdd(a,b,c, n);

	free(a);
	free(b);
	free(c);
	return 0;

}
