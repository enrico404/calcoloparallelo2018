#include <omp.h>
#include <stdio.h>
#include <time.h>
int main(){

    long long int a=1;
    long long int N = 2000000000L;
   
    #pragma omp parallel for schedule(dynamic, N/4)
    for(int i=0; i<N; i++){
       volatile long a = i * 1000000L; //simulo il lavoro dipendente da iterazioni
    }
 
}