#include <stdio.h>
#include <omp.h>
int main(int argc, char const *argv[])
{
    int var=10;
    int n=6;
    #pragma omp parallel num_threads(n) firstprivate(var) 
        {
        if(omp_get_thread_num()==2) var = 5;
        else var++;

        printf("[%d] var: %d\n", omp_get_thread_num(),var);
         
        }
        printf("%d \n", var);
    return 0;
}
