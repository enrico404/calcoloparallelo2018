#include <stdio.h>
#include <omp.h>

int main(int argc, char const *argv[])
{
    #pragma omp parallel
    {
        printf("[%d] Hello world \n", omp_get_thread_num());
        #pragma omp barrier
        printf("[%d] Hello world \n", omp_get_thread_num());

    }
    return 0;
}
