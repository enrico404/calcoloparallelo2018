#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

//3000*3000 lo risolve in 300ms -> openmp 17sec su i5 7600k
#define ROWS 10000
#define COLS 10000
// 10000*10000 = 18.2 secondi

__global__ void molt(int *m1, int *m2, int *c){

    int cvalue = 0;
    int row =  blockIdx.y*blockDim.y + threadIdx.y;
    int col = blockIdx.x*blockDim.x+threadIdx.x;
    if(row < ROWS && col < COLS){
        for(int i=0; i< COLS; i++){
            cvalue += m1[row*COLS+i]*m2[i*COLS+col];
           
        }
    
        c[row*COLS+col] = cvalue;
    }
  
}

void stampa_matrice(int *m){

    for(int i=0;i<ROWS; i++){
        for(int j=0;j<COLS; j++){
            printf("%d ", m[i*ROWS+j]);
        }
        printf("\n");
    }


}

int main(int argc, char const *argv[])
{

        
    int *m1 = (int*)malloc(sizeof(int)*ROWS*COLS);
    int *m2 = (int*)malloc(sizeof(int)*ROWS*COLS);
    int *c  = (int*)malloc(sizeof(int)*ROWS*COLS);

    int *d_m1;
    int *d_m2;
    int *d_c;

    if(ROWS == COLS){


            for(int i=0; i<ROWS; i++){
                for(int j=0; j<COLS; j++){

                    m1[i*ROWS+j] = i+j;
                    m2[i*ROWS+j] = 2*(i+j);
                }
            }
        
        


        cudaMalloc((void**) &d_m1, ROWS*COLS*sizeof(int));
        cudaMalloc((void**) &d_m2, ROWS*COLS*sizeof(int));
        cudaMalloc((void**) &d_c, ROWS*COLS*sizeof(int));

        cudaMemcpy(d_m1, m1, ROWS*COLS*sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_m2, m2, ROWS*COLS*sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_c, c, ROWS*COLS*sizeof(int), cudaMemcpyHostToDevice);

        float threadPerBlock = 16;
        dim3 dimGrid(ceil(ROWS/threadPerBlock), ceil(COLS/threadPerBlock));
        dim3 dimBlock(threadPerBlock, threadPerBlock, 1);

        molt<<<dimGrid, dimBlock>>>(d_m1,d_m2,d_c);

        cudaDeviceSynchronize();
        cudaMemcpy(c, d_c, ROWS*COLS*sizeof(int), cudaMemcpyDeviceToHost);
/* 
        printf("matrice 1: \n");
        stampa_matrice(m1);
        printf("matrice 2: \n");
        stampa_matrice(m2);
        printf("matrice risultante: \n");
        stampa_matrice(c); */





        cudaFree(d_m1);
        cudaFree(d_m2);
        cudaFree(d_c);
        free(m1);
        free(m2);
        free(c);
    }
    return 0;
}
