#include <stdio.h>
#include <omp.h>

void t0(){
    printf(" 0 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}

void t1(){
    printf(" 1 Hello wolrd, i'm the thread: %d \n", omp_get_thread_num());


}
int main(){
#pragma omp parallel num_threads(2)
{
    //va bene anche usare la sections
    #pragma omp single
    {
        #pragma omp task
        {
            t0();
        }

        #pragma omp task
        {
            t1();
        }
    }//implicit barrier
    
}//implicit barrier



return 0;
}