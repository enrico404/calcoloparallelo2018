#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>

pthread_mutex_t mutex; 
void *body(void * arg){

    for(int j=0; j<40; j++){
        //esattamente come setlock di openmp mi garantisce che esegue un solo thread per volta qui dentro
        pthread_mutex_lock(&mutex);
        for(int i=0; i<10000000; i++);
        fprintf(stderr,(char*) arg);
        pthread_mutex_unlock(&mutex);

    }
    printf("\n");

}


int main(){


    int err1,err2,err3;
    pthread_t t1,t2,t3;
    pthread_attr_t attr; 
    pthread_mutexattr_t mutexattr;

    pthread_attr_init(&attr);
    pthread_mutexattr_init(&mutexattr);
    pthread_mutex_init(&mutex, &mutexattr);

    err1 = pthread_create(&t1, &attr, body, (void *)".");
    err2 = pthread_create(&t2, &attr, body, (void *)"*");
    err3 = pthread_create(&t3, &attr, body, (void *)"#");
    
    if(err1 != 0 || err2 != 0 || err3 != 0){
        printf("error on thread creation \n");
        return 1;
    } 
    pthread_attr_destroy(&attr);
    pthread_mutex_destroy(&mutex);
    pthread_mutexattr_destroy(&mutexattr);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
   
    return 0;
}